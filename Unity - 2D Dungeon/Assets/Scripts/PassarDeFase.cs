﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PassarDeFase : MonoBehaviour
{
    [SerializeField] float EsperaParaCarregarFase = 2f;
    [SerializeField] float FatorDeCameraLenta = 0.2f;
    [SerializeField] int ProximaFase;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(CarregarProximaFase());
    }

    IEnumerator CarregarProximaFase()
    {
        Time.timeScale = FatorDeCameraLenta;
        yield return new WaitForSecondsRealtime(EsperaParaCarregarFase);
        Time.timeScale = 1f;

        //int indiceDaFaseAtual = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(ProximaFase);
    }
}
