﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentoDoInimigo : MonoBehaviour
{
    [SerializeField] float velocidadeDeMovimento = 1f;
    Rigidbody2D meuRigibBody2D;
    bool estaOlhandoParaDireita;

    void Start()
    {
        meuRigibBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (EstaOlhandoParaDireita())
        {
            meuRigibBody2D.velocity = new Vector2(velocidadeDeMovimento, 0);
        }
        else
        {
            meuRigibBody2D.velocity = new Vector2(-velocidadeDeMovimento, 0);
        }
    }

    bool EstaOlhandoParaDireita()
    {
        return transform.localScale.x > 0;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        transform.localScale = new Vector2(-(Mathf.Sign(meuRigibBody2D.velocity.x)), 1f);
    }
}
