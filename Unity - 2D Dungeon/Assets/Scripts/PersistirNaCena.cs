﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PersistirNaCena : MonoBehaviour
{
    int cenaInicial;
    private void Awake()
    {
        int qtdDePersistirNaCena = FindObjectsOfType<PersistirNaCena>().Length;
        if (qtdDePersistirNaCena > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        cenaInicial = SceneManager.GetActiveScene().buildIndex;   
    }

    private void Update()
    {
        int cenaAtual = SceneManager.GetActiveScene().buildIndex;

        if (cenaAtual != cenaInicial)
        {
            Destroy(gameObject);
        }
    }
}
