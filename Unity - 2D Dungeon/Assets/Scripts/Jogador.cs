﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Importa os Scripts de CrossPlataformImput
using UnityStandardAssets.CrossPlatformInput;

public class Jogador : MonoBehaviour
{
    Rigidbody2D meuRigidbody;
    [SerializeField] float velocidadeDaCorrida = 5f;
    Animator meuAnimator;
    bool estaVivo = true;
    [SerializeField] float velocidadeDoPulo = 5f;
    CapsuleCollider2D colisorDoCorpo;
    [SerializeField] float velocidadeDeSubida = 5f;
    float gravidadeInicial;
    BoxCollider2D colisorDosPes;

    [SerializeField] Vector2 direçãoDaMorte = new Vector2(25f, 25f);

    // Start is called before the first frame update
    void Start()
    {
        meuRigidbody = GetComponent<Rigidbody2D>();
        meuAnimator = GetComponent<Animator>();
        colisorDoCorpo = GetComponent<CapsuleCollider2D>();
        gravidadeInicial = meuRigidbody.gravityScale;
        colisorDosPes = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (estaVivo == false)
        {
            return;
        }

        Correr();
        GirarSprite();
        Pular();
        SubirEscadas();
        Morrer();
    }

    private void Correr()
    {
        float direcao = CrossPlatformInputManager.GetAxis("Horizontal");
        Vector2 velocidade = new Vector2(direcao*velocidadeDaCorrida, meuRigidbody.velocity.y);
        meuRigidbody.velocity = velocidade;

        bool jogadorTemVelocidadeHorizontal = Mathf.Abs(meuRigidbody.velocity.x) > Mathf.Epsilon;
        meuAnimator.SetBool("Correndo", jogadorTemVelocidadeHorizontal);
    }

    private void GirarSprite()
    {
        bool jogadorTemVelocidadeHorizontal = Mathf.Abs(meuRigidbody.velocity.x) > Mathf.Epsilon;
        if (jogadorTemVelocidadeHorizontal)
        {
            transform.localScale = new Vector2(Mathf.Sign(meuRigidbody.velocity.x), 1f);
        }
    }

    private void Pular()
    {
        if (!colisorDosPes.IsTouchingLayers(LayerMask.GetMask("Chao")))
        {
            return;
        }

        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            Vector2 velocidadeDePuloAdicionar = new Vector2(0f, velocidadeDoPulo);
            meuRigidbody.velocity += velocidadeDePuloAdicionar;
        }
    }

    private void SubirEscadas()
    {
        if (!colisorDosPes.IsTouchingLayers(LayerMask.GetMask("Escada")))
        {
            meuAnimator.SetBool("Escalando", false);
            meuRigidbody.gravityScale = gravidadeInicial;
            return;
        }

        float subir = CrossPlatformInputManager.GetAxis("Vertical");
        Vector2 direcaoDeSubida = new Vector2(meuRigidbody.velocity.x, subir * velocidadeDeSubida);
        meuRigidbody.velocity = direcaoDeSubida;
        meuRigidbody.gravityScale = 0f;

        bool jogadorTemVelocidadeVertical = Mathf.Abs(meuRigidbody.velocity.y) > Mathf.Epsilon;
        meuAnimator.SetBool("Escalando", jogadorTemVelocidadeVertical);
    }

    private void Morrer()
    {
        if (colisorDoCorpo.IsTouchingLayers(LayerMask.GetMask("Inimigo", "Obstaculos")))
        {
            estaVivo = false;
            meuAnimator.SetTrigger("Morrendo");
            GetComponent<Rigidbody2D>().velocity = direçãoDaMorte;
            FindObjectOfType<GerenciadorDeJogo>().ProcessarMorte();
        }
    }
}
